##================================================================================================
                                        Challenges
##================================================================================================
## Description
    This repos contain some challenges in some languages.
    Repo created by Ramon Freire, view profile on [Linkedin](https://www.linkedin.com/in/freirein/).

## ReactJS
    Tic-Tac-Toe: Example of aplication for tic-tac-toe game with time-travel.
    Clima: Weather bar example with API consuming . Using [OpenWeather] free API.

## Compilation and Tests
    Use the folder name to know where you compile or run.
    Some cases the library is mentioned in the code where I import.